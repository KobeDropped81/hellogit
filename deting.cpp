#include <string>
#include <iostream>
using namespace std;

int main(){

	//declarations
	string str1 = "hello";
	string str2 = str1 + " world";
	
	//printing
	cout << "str1 = " << str1 << endl;
	cout << "the 4th character is " << str1[3] << endl;

	return 0;
}
